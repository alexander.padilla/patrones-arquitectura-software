/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import {PolymerElement, html} from '@polymer/polymer/polymer-element.js';

import '../common/spc-data.js';

class DetailView extends PolymerElement {
  static get template() {
    return html `
      <style include="global">
        :host {
          display: block;
        }
        
        .main {
          background-image: url('https://i.imgur.com/3839Q7x.png');
        }
        
        .button {
          background: var(--paper-red-900);
          color: #fff;
        }
        
        .button[disabled] {
          background: var(--paper-grey-100);
          color: var(--paper-grey-900);
        }
        
        .title {
          @apply --paper-font-title;
          color: var(--paper-grey-900);
        }
        
        .subtitle {
          @apply --paper-font-subtitle;
          color: var(--paper-grey-700);
        }
        
        .summary {
          @apply --paper-font-common-base;
          @apply --paper-font-common-nowrap;
          font-size: 16px;
          font-weight: 400;
          line-height: 20px;
          color: var(--paper-grey-800);
        }
        
        .mar-5 {
          margin: 5px;
        }
        
        .mar-10 {
          margin: 10px;
        }
        
        .pad-main {
          padding-bottom: 64px;
          padding-top: 100px;
        }
        
        .wrap {
          white-space: normal;
          text-align: justify;
        }
        
        .section {
          @apply --paper-font-subtitle;
          color: var(--paper-grey-800);
        }
        
        .poster {
          max-width: 325px;
          max-height: 420px;
        }
      </style>

      <div class="main">
        <div class="vertical center h-100 pad-main">
          <div class="horizontal center">
            <span class="title-app">Netflix Inc.</span>
          </div>
          <div class="horizontal center">
            <span class="subtitle-app">Sentiments analyzed</span>
          </div>
          
          <div class="horizontal center">
            <div class="card">
              <div class="horizontal center">
                <div class="vertical center">
                  <span class="title">[[title]]</span>
                </div>
                <div class="vertical center">
                  <span class="subtitle mar-5">[[release]]</span>
                </div>
              </div>
              <div class="horizontal">
                <div class="vertical">
                  <img src="[[poster]]" class="poster"></img>
                </div>
                <div class="vertical center mar-10">
                  <span class="wrap summary">[[summary]]</span>
                  <div class="horizontal justified mar-10">
                    <div class="vertical mar-5">
                      <div class="horizontal center">
                        <span class="section">Director</span>
                      </div>
                      <div class="horizontal center">
                        <span>[[director]]</span>
                      </div>
                    </div>
                    <div class="vertical mar-5">
                      <div class="horizontal center">
                        <span class="section">Actors</span>
                      </div>
                      <div class="horizontal center">
                        <span>[[actors]]</span>
                      </div>
                    </div>
                    <div class="vertical mar-5">
                      <div class="horizontal center">
                        <span class="section">Genre</span>
                      </div>
                      <div class="horizontal center">
                        <span>[[genre]]</span>
                      </div>
                    </div>
                  </div>
                  
                </div>
                
              </div>
            </div>
          </div>
          
          <div class="horizontal center">
            <div class="card">
              <div class="horizontal start">
                <div class="vertical center">
                  <span class="title">Analysis result</span>
                </div>
              </div>
            </div>
          </div>
          
          <div class="horizontal center">
            <div class="card">
              <div class="horizontal start">
                <div class="vertical center">
                  <span class="title">Microservices used</span>
                </div>
              </div>
            </div>
          </div>
          
          <div class="horizontal center mar-10">
            <paper-button class="button" on-tap="_performAnalysis">perform again</paper-button>
          </div>
          
        </div>
      </div>
      
      <spc-dialog id="dialog"></spc-dialog>
      <spc-data id="data"></spc-data>
    `;
  }
  
  static get properties() {
    return {
      movie: {
        type: String,
        value: '',
        notify: true,
        observer: '_requestMovie'
      },
      username: {
        type: String,
        value: '',
        notify: true
      },
      title: {
        type: String,
        value: ''
      },
      release: {
        type: String,
        value: '',
      },
      summary: {
        type: String,
        value: '',
      },
      poster: {
        type: String,
        value: '',
      },
      director: {
        type: String,
        value: '',
      },
      actors: {
        type: String,
        value: '',
      },
      genre: {
        type: String,
        value: '',
      }
    };
  }
  
  ready(){
    super.ready();
    this.$.data.addEventListener('done', e => this._handleResponse(e));
  }
  
  _requestMovie(value){
    if(value !== undefined && value !== ''){
      this.$.dialog.type = 'loader';
      this.$.dialog.text = `loading...`;
      this.$.dialog.open();
      this.$.data.getMovieInfo(this.movie);
    }
  }
  
  _handleResponse(event){
    let query = event.detail.query;
    switch (query) {
      case 'getMovieInfo':
        let movie = event.detail.data;
        console.log(movie);
        this._loadTitle(movie);
      break;
    }
  }
  
  _loadTitle(movie){
    this.title = movie['Title'];
    this.release = movie['Released'];
    this.summary = movie['Plot'];
    this.poster = movie['Poster'];
    this.director = movie['Director'];
    this.actors = movie['Actors'];
    this.genre = movie['Genre'];
    this.$.dialog.close();
  }
  
  _performAnalysis(){
    document.location.href = '/';
  }
}

window.customElements.define('detail-view', DetailView);